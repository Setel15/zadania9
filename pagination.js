const customData = ["lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "sit", "dolor", "lorem", "ipsum", "sit", "dolor", "lorem", "lorem", "lorem", "lorem", "lorem", "ipsum", "ipsum", "ipsum", "ipsum", "ipsum", "sit", "sit", "sit", "sit", "sit", "sit", "sit", "sit", "sit", "dolor", "dolor", "dolor", "dolor", "dolor", "dolor", "dolor", "dolor", "dolor", "dolor", "dolor"]
const customSettings = {
    "actualPageIdx": 1,
    "entriesOnPage": 10
}
const paginateArray = (dataEntries = [], { actualPageIdx = 1, entriesOnPage = 10 }) => {
    if (actualPageIdx <= 0 || entriesOnPage <= 0 || dataEntries == []) return new Error("entered data is invalid")
    let paginatedData = []
    for (let i = 0; i < (dataEntries.length / entriesOnPage); i++) {
        paginatedData.push(dataEntries.slice(i * entriesOnPage, (i + 1) * entriesOnPage))
    }
    return paginatedData[actualPageIdx - 1] || new Error("entered data is invalid, it's probably a problem with page index") //jeśli strony numerowane są od 0, a nie 1 to funkcja powinna zwracać paginatedData[actualPageIdx]
}
console.log(paginateArray(customData, customSettings))